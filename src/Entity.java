import bagel.*;
import java.lang.Math;
import java.security.PrivateKey;

public class Entity {
    private double PositionX;
    private double PositionY;
    private final Image image;

    // set up entity
    public Entity(double PositionX, double PositionY, String image ){
        this.image = new Image(image);
        this.PositionX = PositionX;
        this.PositionY = PositionY;
    };

    // to draw the entity in game
    public void drawEntity(){
        this.image.draw(this.getPositionX(), this.getPositionY());
    }

    // get position
    public double getPositionX(){
        return this.PositionX;
    }
    public double getPositionY(){
        return  this.PositionY;
    }

   // set up position
    public void setPositionX(double x){
        this.PositionX = x;
    }
    public void  setPositionY(double y){
        this.PositionY = y;
    }

    // calculate the distance between two entity
    public double distanceFrom(Entity ohterEntity){
        double dx = this.getPositionX() - ohterEntity.getPositionX();
        double dy = this.getPositionY() - ohterEntity.getPositionY();
        return Math.sqrt(dx*dx + dy*dy);
    }


}
