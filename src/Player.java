public class Player extends Entity {
    private int energyLevel;
    private static final int stepSize = 10;

    // set up the player
    public Player(double PositionX, double PositionY, String image, int energyLevel) {
        super(PositionX, PositionY, image);
        this.energyLevel = energyLevel;
    }

    // get energy level from player
    public int getEnergyLevel() {
        return this.energyLevel;
    }

    // set energy level
    public void setEnergyLevel(int energyLeve){
        this.energyLevel = energyLeve;
    }

    // check meet to other entity
    public boolean meet(Entity entity){
        if(this.distanceFrom(entity) < 50){
            return true;
        }
        return false;
    }

    // move to other entity
    public void moveTo(Entity entity){
        double xd = (entity.getPositionX() - this.getPositionX())/this.distanceFrom(entity);
        double yd = (entity.getPositionY() - this.getPositionY())/this.distanceFrom(entity);

        this.setPositionX(this.getPositionX() + xd*this.stepSize);
        this.setPositionY(this.getPositionY() + yd*this.stepSize);
    }
}
