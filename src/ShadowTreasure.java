import bagel.*;
import org.lwjgl.system.CallbackI;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;


/**
 * An example Bagel game.
 */
public class ShadowTreasure extends AbstractGame {

    // for rounding double number; use this to print the location of the player
    private static DecimalFormat df = new DecimalFormat("0.00");


    // Set up the attributes for game
    private Player player;
    private Zambie zambie;
    private Sandwich sandwich;
    private Background background;
    private int countFrame = 0;
    private Font font;

    public static void printInfo(double x, double y, int e) {
        System.out.println(df.format(x) + "," + df.format(y) + "," + e);
    }
    
    public ShadowTreasure() throws IOException {

        this.loadEnvironment("test/test2/environment.csv");
        // Add code to initialize other attributes as needed
    }

    /**
     * Load from input file
     */
    private void loadEnvironment(String filename){
        // Code here to read from the file and set up the environment
        try(BufferedReader file = new BufferedReader(new FileReader(filename))){
            String[] newLine = file.readLine().split(",");

            //read each row from file and set up the entities and background
            this.player = new Player(Double.parseDouble(newLine[1]),Double.parseDouble(newLine[2]), "res/images/player.png", Integer.parseInt(newLine[3]));
            newLine = file.readLine().split(",");
            this.zambie = new Zambie(Double.parseDouble(newLine[1]),Double.parseDouble(newLine[2]), "res/images/zombie.png");
            newLine = file.readLine().split(",");
            this.sandwich = new Sandwich(Double.parseDouble(newLine[1]),Double.parseDouble(newLine[2]), "res/images/sandwich.png");
            this.background = new Background("res/images/background.png");
            this.font = new Font( "res/font/DejaVuSans-Bold.ttf",20);

            // draw the attributes and energy level
            this.background.drawBackground();
            this.player.drawEntity();
            this.zambie.drawEntity();
            this.sandwich.drawEntity();
            this.font.drawString("energy:" + player.getEnergyLevel(), 20, 760);

        }catch(IOException e){
            e.printStackTrace();
        }

    }

    /**
     * Performs a state update.
     */
    @Override
    public void update(Input input) {
        // Logic to update the game, as per specification must go here

        // draw the attributes and energy level
        this.background.drawBackground();
        this.player.drawEntity();
        this.zambie.drawEntity();
        if(this.sandwich != null) {
            this.sandwich.drawEntity();
        }
        this.font.drawString("energy:" + this.player.getEnergyLevel(), 20, 760);

        // follow the interaction logic and update energy in each tick, than output the location and energy level
        if( this.countFrame == 10){
            if( this.player.meet(this.zambie)){
                this.player.setEnergyLevel(this.player.getEnergyLevel() - 3);
                printInfo(this.player.getPositionX(), this.player.getPositionY(), this.player.getEnergyLevel());
                System.exit(0);
            }else if(this.sandwich != null && this.player.meet(this.sandwich)){
                this.player.setEnergyLevel(this.player.getEnergyLevel() + 5);
                printInfo(this.player.getPositionX(), this.player.getPositionY(), this.player.getEnergyLevel());
                this.sandwich = null;
            }else{
                printInfo(this.player.getPositionX(), this.player.getPositionY(), this.player.getEnergyLevel());
            }
            if( this.player.getEnergyLevel() >= 3){
                this.player.moveTo(this.zambie);
            }else{
                this.player.moveTo(this.sandwich);
            }
            this.countFrame = 0;
        }
        this.countFrame += 1;
    }


    /**
     * The entry point for the program.
     */
    public static void main(String[] args) throws IOException {

        ShadowTreasure game = new ShadowTreasure();
        game.run();
    }
}
