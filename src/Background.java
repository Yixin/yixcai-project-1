import bagel.*;

public class Background{
    Image image;

    public Background(String image) {
        this.image = new Image(image);
    }

    // to draw the background of game
    public void drawBackground(){
        image.drawFromTopLeft(0,0);
    }
}